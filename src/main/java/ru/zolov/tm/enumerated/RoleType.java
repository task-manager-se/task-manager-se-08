package ru.zolov.tm.enumerated;

public enum RoleType {
  ADMIN("ADMIN"), USER("USER");
  
  private String displayName;
  
  RoleType(String displayName) {
    this.displayName = displayName;
  }
  
  public String getDisplayName() {
    return displayName;
  }
  
}
