package ru.zolov.tm.service;

import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectService extends AbstractService<Project> implements IProjectService {
  
  private final IProjectRepository projectRepository;
  private final ITaskRepository taskRepository;
  
  public ProjectService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
    this.projectRepository = projectRepository;
    this.taskRepository = taskRepository;
  }
  
  @Override
  public IProjectRepository getProjectRepository() {
    return projectRepository;
  }
  
  @Override
  public ITaskRepository getTaskRepository() {
    return taskRepository;
  }
  
  @Override
  public void create(@NotNull final String userId, @NotNull final String name,
    @NotNull final String description, @Nullable Date start, @Nullable Date finish)
    throws EmptyStringException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (name == null || name.isEmpty()) {
      throw new EmptyStringException();
    }
    Project project = new Project();
    project.setName(name);
    project.setUserId(userId);
    project.setDescription(description);
    project.setDateOfStart(start);
    project.setDateOfFinish(finish);
    if (description == null || description.isEmpty()) {
      project.setDescription("empty");
    }
    projectRepository.persist(project);
  }
  
  @Override
  public Project read(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    if (projectRepository.findOne(userId, id) == null) {
      throw new EmptyRepositoryException();
    }
    return projectRepository.findOne(userId, id);
  }
  
  @Override
  public List<Project> readAll(String userId) {
    return projectRepository.findAll(userId);
  }
  
  @Override
  public void update(@NotNull final String userId, @NotNull final String id,
    @NotNull final String name, @NotNull final String description, @Nullable Date start,
    @Nullable Date finish) {
    if (userId == null || userId.isEmpty()) {
      return;
    }
    if (id == null || id.isEmpty()) {
      return;
    }
    if (name == null || name.isEmpty()) {
      return;
    }
    
    projectRepository.update(userId, id, name, description, start, finish);
  }
  
  @Override
  public boolean remove(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    taskRepository.removeAllByProjectID(userId, id);
    return projectRepository.remove(userId, id);
  }
}
