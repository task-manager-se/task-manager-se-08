package ru.zolov.tm.service;

import java.text.SimpleDateFormat;

public abstract class AbstractService<E> {
  
  public final SimpleDateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY");
  
}
