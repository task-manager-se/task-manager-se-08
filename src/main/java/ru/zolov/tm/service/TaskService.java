package ru.zolov.tm.service;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskService extends AbstractService<Task> implements ITaskService {
  
  private ITaskRepository taskRepository;
  
  public TaskService(ITaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }
  
  @Override
  public ITaskRepository getTaskRepository() {
    return taskRepository;
  }
  
  @Override
  public void create(
    @NotNull final String userId, @NotNull final String projectId,
    @NotNull final String name, @NotNull final String description
  ) throws EmptyStringException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (name == null || name.isEmpty()) {
      throw new EmptyStringException();
    }
    if (projectId == null || projectId.isEmpty()) {
      throw new EmptyStringException();
    }
    Task task = new Task();
    task.setUserId(userId);
    task.setProjectId(projectId);
    task.setName(name);
    task.setDescription(description);
    taskRepository.persist(task);
  }
  
  @Override
  public List<Task> readAll(String userId) throws EmptyRepositoryException {
    if (taskRepository.findAll() == null || taskRepository.findAll().isEmpty()) {
      throw new EmptyRepositoryException();
    }
    return taskRepository.findAll();
  }
  
  @Override
  public List<Task> readTaskByProjectId(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    if (taskRepository.findTaskByProjId(id) == null) {
      throw new EmptyRepositoryException();
    }
    return taskRepository.findTaskByProjId(id);
  }
  
  @Override
  public Task readTaskById(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    return taskRepository.findOne(userId, id);
  }
  
  @Override
  public boolean remove(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException {
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    return taskRepository.remove(id);
  }
  
  @Override
  public void update(@NotNull final String userId, @NotNull final String id,
    @NotNull final String description) throws EmptyStringException {
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    taskRepository.update(id, description);
  }
  
}
