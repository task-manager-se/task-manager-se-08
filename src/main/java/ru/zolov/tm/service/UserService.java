package ru.zolov.tm.service;

import java.util.Arrays;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.util.hashUtil;

public class UserService extends AbstractService<User> implements IUserService {
  
  private final IUserRepository userRepository;
  private User currentUser = null;
  
  public UserService(IUserRepository userRepository) {
    this.userRepository = userRepository;
  }
  
  @Override
  public User getCurrentUser() {
    return currentUser;
  }
  
  @Override
  public void setCurrentUser(@NotNull final User currentUser) {
    this.currentUser = currentUser;
  }
  
  @Override
  public boolean isAuth() {
    return currentUser != null;
  }
  
  @Override
  public User login(@NotNull final String login, @NotNull final String password)
    throws EmptyStringException, UserNotFoundException {
    if (login == null || login.isEmpty()) {
      throw new EmptyStringException();
    }
    if (password == null || password.isEmpty()) {
      throw new EmptyStringException();
    }
    User user = userRepository.findByLogin(login);
    if (user == null) {
      throw new UserNotFoundException();
    }
    String passwordHash = hashUtil.getHash(password);
    if (passwordHash.equals(user.getPasswordHash())) {
      return user;
    } else {
      return null;
    }
  }
  
  @Override
  public void userRegistration(@NotNull final String login, @NotNull final String password)
    throws EmptyStringException, UserExistException {
    if (login == null || login.isEmpty()) {
      throw new EmptyStringException();
    }
    if (password == null || password.isEmpty()) {
      throw new EmptyStringException();
    }
    if (userRepository.findByLogin(login) != null) {
      throw new UserExistException();
    }
    User newUser = new User();
    newUser.setLogin(login);
    newUser.setRole(RoleType.USER);
    newUser.setPasswordHash(hashUtil.getHash(password));
    userRepository.persist(newUser);
  }
  
  @Override
  public void adminRegistration(@NotNull final String login, @NotNull final String password)
    throws EmptyStringException {
    if (login == null || login.isEmpty()) {
      throw new EmptyStringException();
    }
    if (password == null || password.isEmpty()) {
      throw new EmptyStringException();
    }
    if (userRepository.findByLogin(login) != null) {
      throw new EmptyStringException();
    }
    User admin = new User();
    admin.setLogin(login);
    admin.setRole(RoleType.ADMIN);
    admin.setPasswordHash(hashUtil.getHash(password));
    userRepository.persist(admin);
  }
  
  @Override
  public void logOut() {
    currentUser = null;
  }
  
  @Override
  public boolean isRolesAllowed(@NotNull final RoleType... roleTypes) {
    if (roleTypes == null) {
      return false;
    }
    if (currentUser == null || currentUser.getRole() == null) {
      return false;
    }
    final List<RoleType> types = Arrays.asList(roleTypes);
    return types.contains(currentUser.getRole());
  }
  
  @Override
  public User findByLogin(@NotNull final String login) throws EmptyStringException {
    if (login == null || login.isEmpty()) {
      throw new EmptyStringException();
    }
    return userRepository.findByLogin(login);
  }
  
  @Override
  public boolean remove(@NotNull final String id) throws EmptyStringException {
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    userRepository.remove(id);
    return userRepository.remove(id);
  }
  
  @Override
  public void updateUserPassword(@NotNull final String id, @NotNull final String password)
    throws EmptyStringException {
    User user = userRepository.findOne(id);
    if (id == null || id.isEmpty()) {
      throw new EmptyStringException();
    }
    if (password == null || password.isEmpty()) {
      throw new EmptyStringException();
    }
    user.setPasswordHash(hashUtil.getHash(password));
    userRepository.merge(user);
  }
  
}
