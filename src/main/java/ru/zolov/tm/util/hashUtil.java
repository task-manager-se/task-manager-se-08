package ru.zolov.tm.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class hashUtil {
  
  private hashUtil() {
  }
  
  public static String getHash(String pass) {
    
    MessageDigest digest = null;
    try {
      digest = MessageDigest.getInstance("MD5");
      digest.update(pass.getBytes(StandardCharsets.UTF_8));
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    String md5Hex = new BigInteger(1, digest.digest()).toString(16);
    while (md5Hex.length() < 32) {
      md5Hex = "0" + md5Hex;
    }
    return md5Hex;
  }
}
