package ru.zolov.tm.loader;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;
import ru.zolov.tm.service.UserService;

public final class Bootstrap implements ServiceLocator {
  
  private final IProjectRepository projectRepository = new ProjectRepository();
  private final ITaskRepository taskRepository = new TaskRepository();
  private final IProjectService projectService = new ProjectService(projectRepository,
    taskRepository);
  private final ITaskService taskService = new TaskService(taskRepository);
  private final TerminalService terminalService = new TerminalService();
  private final IUserRepository userRepository = new UserRepository();
  private final IUserService userService = new UserService(userRepository);
  private Map<String, AbstractCommand> commands = new LinkedHashMap<>();
  
  @Override
  public List<AbstractCommand> getCommandList() {
    return new ArrayList<>(commands.values());
  }
  
  @Override
  public IUserService getUserService() {
    return userService;
  }
  
  @Override
  public IProjectService getProjectService() {
    return projectService;
  }
  
  @Override
  public ITaskService getTaskService() {
    return taskService;
  }
  
  @Override
  public TerminalService getTerminalService() {
    return terminalService;
  }
  
  public void registry(AbstractCommand command)
    throws CommandCorruptException, EmptyStringException {
    @NotNull final String cliCommand = command.getName();
    @NotNull final String cliDescription = command.getDescription();
    if (cliCommand == null || cliCommand.isEmpty()) {
      throw new EmptyStringException();
    }
    if (cliDescription == null || cliDescription.isEmpty()) {
      throw new EmptyStringException();
    }
    command.setServiceLocator(this);
    commands.put(cliCommand, command);
  }
  
  public void registry(@NotNull final Class... classes) throws Exception {
    for (Class clazz : classes) {
      registry(clazz);
    }
  }
  
  public void registry(@NotNull final Class clazz) throws Exception {
    if (!AbstractCommand.class.isAssignableFrom(clazz)) {
      return;
    }
    Object command = clazz.getDeclaredConstructor().newInstance();
    AbstractCommand abstractCommand = (AbstractCommand) command;
    registry(abstractCommand);
  }
  
  public void init(@NotNull final Class... classes) throws Exception {
    if (classes == null || classes.length == 0) {
      throw new Exception("Empty list");
    }
    registry(classes);
    initAdmin();
    start();
  }
  
  public void start()
    throws CommandCorruptException, EmptyStringException, EmptyRepositoryException, UserExistException, UserNotFoundException {
    System.out.println("+-------------------------------+");
    System.out.println("|    Welcome to Task manager    |");
    System.out.println("+-------------------------------+");
    
    String command = "";
    
    while (true) {
      try {
        System.out.print("Enter command: \n");
        command = terminalService.nextLine();
        execute(command);
      } catch (CommandCorruptException e) {
        e.printStackTrace();
      }
    }
  }
  
  private void execute(String command)
    throws CommandCorruptException, UserNotFoundException, EmptyRepositoryException, EmptyStringException, UserExistException {
    final AbstractCommand abstractCommand = commands.get(command);
    final boolean rolesAllowed = getUserService().isRolesAllowed(abstractCommand.roles());
    final boolean secureCheck =
      abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth()
        && rolesAllowed);
    if (abstractCommand == null) {
      throw new CommandCorruptException();
    }
    if (secureCheck) {
      abstractCommand.execute();
    } else {
      System.out.println("Error! Access forbidden for this command");
    }
  }
  
  private void initAdmin() {
    try {
      userService.adminRegistration("admin", "admin");
      userService.userRegistration("user", "user");
    } catch (Exception e) {
      System.out.println("Something went wrong in initAdmin!");
      e.printStackTrace();
    }
  }
}


