package ru.zolov.tm.command.project;

import java.util.Date;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.RoleType;

public final class ProjectEditCommand extends AbstractCommand {
  
  private final String name = "project-edit";
  private final String description = "Edit project";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    for (@NotNull final Project project : serviceLocator.getProjectService().readAll(userId)) {
      System.out.println(project);
    }
    System.out.print("Enter project id: ");
    final String id = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter new description: ");
    final String description = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getProjectService()
      .update(userId, id, name, description, new Date(), new Date());
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
