package ru.zolov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.RoleType;

public final class ProjectDisplayCommand extends AbstractCommand {
  
  private final String name = "project-display";
  private final String description = "Display project list";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    for (@NotNull final Project project : serviceLocator.getProjectService().readAll(userId)) {
      System.out.println(project);
    }
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
