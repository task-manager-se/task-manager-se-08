package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

public abstract class AbstractCommand {
  
  protected ServiceLocator serviceLocator;
  
  public void setServiceLocator(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }
  
  public abstract String getName();
  
  public abstract String getDescription();
  
  public abstract boolean secure();
  
  public abstract void execute()
    throws CommandCorruptException, EmptyStringException, EmptyRepositoryException, UserNotFoundException, UserExistException;
  
  public abstract RoleType[] roles();
}
