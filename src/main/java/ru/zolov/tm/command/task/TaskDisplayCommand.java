package ru.zolov.tm.command.task;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class TaskDisplayCommand extends AbstractCommand {
  
  private final String name = "task-display";
  private final String description = "Display task list";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() throws EmptyStringException, EmptyRepositoryException {
    final String user = serviceLocator.getUserService().getCurrentUser().getId();
    System.out.print("Enter project id: ");
    final String id = serviceLocator.getTerminalService().nextLine();
    for (Task task : serviceLocator.getTaskService().readTaskByProjectId(user, id)) {
      System.out.println(task);
    }
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
