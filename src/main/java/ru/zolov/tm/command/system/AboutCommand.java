package ru.zolov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.apache.log4j.BasicConfigurator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;

public class AboutCommand extends AbstractCommand {
  
  private final String name = "about";
  private final String description = "Show information about project and developer";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return true;
  }
  
  @Override
  public void execute() throws CommandCorruptException {
    BasicConfigurator.configure();
    System.out.println(
      " Developer: " + Manifests.read("Developer") +
        "\n Version: " + Manifests.read("Version") +
        "\n BuildNumber (" + Manifests.read("BuildNumber") + ")");
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
