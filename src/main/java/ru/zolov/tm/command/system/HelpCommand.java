package ru.zolov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;

public final class HelpCommand extends AbstractCommand {
  
  private final String name = "help";
  private final String description = "Show all supported commands";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return true;
  }
  
  @Override
  public void execute() {
    for (@NotNull final AbstractCommand command : serviceLocator.getCommandList()) {
      System.out.println(command.getName() + ": " + command.getDescription());
    }
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
