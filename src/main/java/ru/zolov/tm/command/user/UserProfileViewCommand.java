package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;

public class UserProfileViewCommand extends AbstractCommand {
  
  private final String name = "user-profile";
  private final String description = "View user profile";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() {
    System.out.println(
      "Login " + serviceLocator.getUserService().getCurrentUser().getLogin() + ",  userId: "
        + serviceLocator.getUserService().getCurrentUser().getId()
        + ", userRole: " + serviceLocator.getUserService().getCurrentUser().getRole());
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
