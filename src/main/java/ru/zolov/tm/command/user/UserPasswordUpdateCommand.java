package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;

public class UserPasswordUpdateCommand extends AbstractCommand {
  
  private final String name = "user-password-upd";
  private final String description = "Update password";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() throws EmptyStringException {
    final String user = serviceLocator.getUserService().getCurrentUser().getId();
    System.out.println("Enter new password: ");
    final String password = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getUserService().updateUserPassword(user, password);
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
