package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;

public class UserLogOutCommand extends AbstractCommand {
  
  private final String name = "user-logout";
  private final String description = "User logout";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() {
    final String user = serviceLocator.getUserService().getCurrentUser().getLogin();
    serviceLocator.getUserService().logOut();
    
    System.out.println("Пользователь " + user + " вышел из системы.");
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
