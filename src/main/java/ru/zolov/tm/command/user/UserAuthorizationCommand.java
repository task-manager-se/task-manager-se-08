package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserNotFoundException;

public class UserAuthorizationCommand extends AbstractCommand {
    
    private final String name = "user-auth";
    private final String description = "Auth user";
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public String getDescription() {
        return description;
    }
    
    @Override
    public boolean secure() {
        return true;
    }
    
    @Override
    public void execute() throws EmptyStringException, UserNotFoundException {
        System.out.println("Please enter login: ");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password: ");
        final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService()
          .setCurrentUser(serviceLocator.getUserService().login(login, password));
        if (serviceLocator.getUserService().isAuth()) {
            System.out.println("Welcome, " + login + "!");
        }
        
    }
    
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}
