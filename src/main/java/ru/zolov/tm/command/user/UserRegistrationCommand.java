package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;

public class UserRegistrationCommand extends AbstractCommand {
  
  private final String name = "user-registration";
  private final String description = "New user registration";
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
  
  @Override
  public boolean secure() {
    return false;
  }
  
  @Override
  public void execute() throws EmptyStringException, UserExistException {
    System.out.println("Enter login : ");
    final String login = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter password : ");
    String password = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getUserService().userRegistration(login, password);
    System.out.println("User " + login + " successfully added!");
    System.out.println("Something went wrong in registration command!");
  }
  
  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
