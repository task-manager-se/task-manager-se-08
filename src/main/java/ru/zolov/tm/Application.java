package ru.zolov.tm;

import ru.zolov.tm.command.project.ProjectCreateCommand;
import ru.zolov.tm.command.project.ProjectDisplayCommand;
import ru.zolov.tm.command.project.ProjectEditCommand;
import ru.zolov.tm.command.project.ProjectRemoveCommand;
import ru.zolov.tm.command.system.AboutCommand;
import ru.zolov.tm.command.system.ExitCommand;
import ru.zolov.tm.command.system.HelpCommand;
import ru.zolov.tm.command.task.TaskCreateCommand;
import ru.zolov.tm.command.task.TaskDisplayCommand;
import ru.zolov.tm.command.task.TaskEditCommand;
import ru.zolov.tm.command.task.TaskRemoveCommand;
import ru.zolov.tm.command.user.UserAuthorizationCommand;
import ru.zolov.tm.command.user.UserLogOutCommand;
import ru.zolov.tm.command.user.UserPasswordUpdateCommand;
import ru.zolov.tm.command.user.UserProfileViewCommand;
import ru.zolov.tm.command.user.UserRegistrationCommand;
import ru.zolov.tm.loader.Bootstrap;

public class Application {
  
  public static final Class<?>[] CLASSES = {AboutCommand.class,
    ExitCommand.class,
    HelpCommand.class,
    ProjectCreateCommand.class,
    ProjectDisplayCommand.class,
    ProjectEditCommand.class,
    ProjectRemoveCommand.class,
    TaskCreateCommand.class,
    TaskDisplayCommand.class,
    TaskEditCommand.class,
    TaskRemoveCommand.class,
    UserAuthorizationCommand.class,
    UserLogOutCommand.class,
    UserPasswordUpdateCommand.class,
    UserProfileViewCommand.class,
    UserRegistrationCommand.class};
  
  public static void main(String[] args) throws Exception {
    Bootstrap bootstrap = new Bootstrap();
    bootstrap.init(CLASSES);
  }
}

