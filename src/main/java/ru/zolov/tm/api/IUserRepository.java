package ru.zolov.tm.api;

import java.util.List;
import ru.zolov.tm.entity.User;

public interface IUserRepository {
  
  void persist(User user);
  
  List<User> findAll();
  
  User findOne(String id);
  
  User findByLogin(String login);
  
  void merge(User user);
  
  boolean remove(String id);
  
  void removeAll();
  
  void update(String id, String login);
}
