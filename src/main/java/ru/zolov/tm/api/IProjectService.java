package ru.zolov.tm.api;

import java.util.Date;
import java.util.List;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectService {
  
  IProjectRepository getProjectRepository();
  
  ITaskRepository getTaskRepository();
  
  void create(String userId, String name, String description, Date start, Date finish)
    throws EmptyStringException, EmptyRepositoryException;
  
  Project read(String userId, String id) throws EmptyStringException, EmptyRepositoryException;
  
  List<Project> readAll(String userId);
  
  void update(String userId, String id, String name, String description, Date start, Date finish);
  
  boolean remove(String userId, String id) throws EmptyStringException, EmptyRepositoryException;
}
