package ru.zolov.tm.api;

import java.util.List;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.service.TerminalService;

public interface ServiceLocator {
  
  IProjectService getProjectService();
  
  ITaskService getTaskService();
  
  TerminalService getTerminalService();
  
  List<AbstractCommand> getCommandList();
  
  IUserService getUserService();
}
