package ru.zolov.tm.api;

import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

public interface IUserService {
  
  User getCurrentUser();
  
  void setCurrentUser(User currentUser);
  
  boolean isAuth();
  
  User login(String login, String password) throws EmptyStringException, UserNotFoundException;
  
  void userRegistration(String login, String password)
    throws EmptyStringException, UserExistException;
  
  void adminRegistration(String login, String password) throws EmptyStringException;
  
  void logOut();
  
  boolean isRolesAllowed(RoleType... roleTypes);
  
  User findByLogin(String login) throws EmptyStringException;
  
  boolean remove(String id) throws EmptyStringException;
  
  void updateUserPassword(String id, String password) throws EmptyStringException;
}
