package ru.zolov.tm.api;

import java.util.List;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskRepository {
  
  void persist(String userId, String id, String name);
  
  void persist(Task task);
  
  Task findOne(String userId, String id)
    throws EmptyStringException, EmptyRepositoryException;
  
  List<Task> findAll();
  
  List<Task> findTaskByProjId(String id) throws EmptyStringException, EmptyRepositoryException;
  
  List<Task> findAllByUserId(String userId);
  
  void update(String id, String description);
  
  boolean remove(String id);
  
  void removeAll();
  
  boolean removeAllByProjectID(String userId, String id);
  
  void merge(Task task);
}
