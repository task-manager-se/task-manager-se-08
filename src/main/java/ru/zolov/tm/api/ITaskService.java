package ru.zolov.tm.api;

import java.util.List;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskService {
  
  ITaskRepository getTaskRepository();
  
  void create(String userId, String projectId, String name, String description)
    throws EmptyStringException, EmptyRepositoryException;
  
  List<Task> readAll(String userId) throws EmptyStringException, EmptyRepositoryException;
  
  List<Task> readTaskByProjectId(String userId, String id)
    throws EmptyStringException, EmptyRepositoryException;
  
  Task readTaskById(String userId, String id) throws EmptyStringException, EmptyRepositoryException;
  
  boolean remove(String userId, String id) throws EmptyStringException, EmptyRepositoryException;
  
  void update(String userId, String id, String description)
    throws EmptyStringException, EmptyRepositoryException;
}
