package ru.zolov.tm.api;

import java.util.Date;
import java.util.List;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectRepository {
  
  void persist(Project project);
  
  Project findOne(String userId, String id) throws EmptyStringException, EmptyRepositoryException;
  
  List<Project> findAll(String userId);
  
  List<Project> findAllByUserId(String userId);
  
  void update(String userId, String id, String name, String description, Date start, Date finish);
  
  boolean remove(String userId, String id);
  
  void removeAll();
  
  void merge(Project project);
}
