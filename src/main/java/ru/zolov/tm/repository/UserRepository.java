package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.entity.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {
  
  @Override
  public void persist(@NotNull final User user) {
    storage.put(user.getId(), user);
  }
  
  @Override
  public List<User> findAll() {
    final List<User> usersList;
    usersList = new ArrayList<>(storage.values());
    return usersList;
  }
  
  @Override
  public User findOne(@NotNull final String id) {
    return storage.get(id);
  }
  
  @Override
  public User findByLogin(@NotNull final String login) {
    User foundedUser = null;
    for (User user : storage.values()) {
      if (user.getLogin().equals(login)) {
        foundedUser = user;
      }
    }
    return foundedUser;
  }
  
  @Override
  public void merge(@NotNull final User user) {
    if (storage.containsKey(user.getId())) {
      update(user.getId(), user.getLogin());
    } else {
      persist(user);
    }
  }
  
  @Override
  public boolean remove(@NotNull final String id) {
    return storage.remove(id) != null;
  }
  
  @Override
  public void removeAll() {
    storage.clear();
  }
  
  @Override
  public void update(@NotNull final String id, final String login) {
    final User user = storage.get(id);
    user.setLogin(login);
  }
}
