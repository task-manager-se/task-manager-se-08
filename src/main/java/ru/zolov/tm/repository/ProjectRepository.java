package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
  
  @Override
  public void persist(@NotNull final Project project) {
    storage.put(project.getId(), project);
  }
  
  @Override
  public Project findOne(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    Project project = storage.get(id);
    if (project == null) {
      throw new EmptyRepositoryException();
    }
    if (!project.getUserId().equals(userId)) {
      throw new EmptyStringException();
    }
    return project;
  }
  
  @Override
  public List<Project> findAll(@NotNull final String userId) {
    List<Project> projectList = new ArrayList<>();
    for (Project project : storage.values()) {
      if (project.getUserId().equals(userId)) {
        projectList.add(project);
      }
    }
    return projectList;
  }
  
  @Override
  public List<Project> findAllByUserId(@NotNull final String userId) {
    List<Project> projectList = new ArrayList<>();
    for (Project project : storage.values()) {
      if (project.getUserId().equals(userId)) {
        projectList.add(project);
      }
    }
    return projectList;
  }
  
  @Override
  public void update(@NotNull final String userId, @NotNull final String id,
    @NotNull final String name, @NotNull final String description, @Nullable final Date start,
    @Nullable final Date finish) {
    Project project = storage.get(id);
    project.setUserId(userId);
    project.setName(name);
    project.setDescription(description);
    project.setDateOfStart(start);
    project.setDateOfFinish(finish);
  }
  
  @Override
  public boolean remove(@NotNull String userId, @NotNull String id) {
    if (storage.get(id).getUserId().equals(userId)) {
      storage.remove(id);
      return true;
    }
    return false;
  }
  
  @Override
  public void removeAll() {
    storage.clear();
  }
  
  @Override
  public void merge(@NotNull Project project) {
    if (storage.containsKey(project.getId())) {
      update(project.getUserId(), project.getId(), project.getName(), project.getDescription(),
        project.getDateOfStart(), project.getDateOfFinish());
    } else {
      persist(project);
    }
  }
}
