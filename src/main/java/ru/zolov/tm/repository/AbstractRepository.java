package ru.zolov.tm.repository;

import java.util.LinkedHashMap;
import java.util.Map;
import org.jetbrains.annotations.NotNull;

abstract class AbstractRepository<E> {
  
  Map<String, E> storage = new LinkedHashMap<>();
  
  abstract void persist(@NotNull final E entity);
  
  abstract void merge(@NotNull final E entity);
}
