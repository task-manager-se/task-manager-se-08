package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
  
  @Override
  public void persist(@NotNull final String userId, @NotNull final String projectId,
    @NotNull final String name) {
    Task task = new Task();
    task.setUserId(userId);
    task.setProjectId(projectId);
    task.setName(name);
    storage.put(task.getId(), task);
  }
  
  @Override
  public void persist(@NotNull final Task task) {
    storage.put(task.getId(), task);
  }
  
  @Override
  public Task findOne(@NotNull final String userId, @NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    Task task = storage.get(id);
    if (task == null) {
      throw new EmptyRepositoryException();
    }
    if (!task.getUserId().equals(userId)) {
      throw new EmptyStringException();
    }
    return task;
  }
  
  @Override
  public List<Task> findAll() {
    return new ArrayList<>(storage.values());
  }
  
  @Override
  public List<Task> findTaskByProjId(@NotNull final String id)
    throws EmptyStringException, EmptyRepositoryException {
    List<Task> result = new ArrayList<>();
    
    for (@NotNull final Task task : storage.values()) {
      if (task == null) {
        throw new EmptyRepositoryException();
      }
      if (task.getProjectId().equals(id)) {
        result.add(task);
      }
    }
    return result;
  }
  
  @Override
  public List<Task> findAllByUserId(@NotNull final String userId) {
    List<Task> taskList = new ArrayList<>();
    for (Task task : storage.values()) {
      if (task.getUserId().equals(userId)) {
        taskList.add(task);
      }
    }
    return taskList;
  }
  
  @Override
  public void update(@NotNull final String id, @NotNull final String description) {
    Task task = storage.get(id);
    task.setName(description);
  }
  
  @Override
  public boolean remove(@NotNull final String id) {
    return storage.remove(id) != null;
  }
  
  @Override
  public void removeAll() {
    storage.clear();
  }
  
  @Override
  public boolean removeAllByProjectID(@NotNull final String userId, @NotNull final String id) {
    for (Task task : storage.values()) {
      if (task.getUserId().equals(userId) && task.getProjectId().equals(id)) {
        storage.remove(task.getId());
        return true;
      }
    }
    return false;
  }
  
  @Override
  public void merge(@NotNull final Task task) {
    if (storage.containsKey(task.getId())) {
      update(task.getId(), task.getDescription());
    } else {
      persist(task);
    }
  }
}


