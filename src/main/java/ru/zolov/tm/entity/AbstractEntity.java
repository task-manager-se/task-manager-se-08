package ru.zolov.tm.entity;

import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@NoArgsConstructor
public abstract class AbstractEntity {
  
  @NotNull
  public String id = UUID.randomUUID().toString();
}
