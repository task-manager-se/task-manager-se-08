package ru.zolov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {
  
  @NotNull
  private String login = "";
  @NotNull
  private String passwordHash = "";
  @NotNull
  private RoleType role = RoleType.USER;
}
