package ru.zolov.tm.entity;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntity {
  
  @NotNull
  private String userId;
  @NotNull
  private String projectId;
  @NotNull
  private String name = "";
  @NotNull
  private String description = "empty";
  @Nullable
  private Date dateOfStart;
  @Nullable
  private Date dateOfFinish;
  
  @Override
  public String toString() {
    return "Task{" +
      "id='" + id + '\'' +
      ", userId='" + userId + '\'' +
      ", projectId='" + projectId + '\'' +
      ", name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", dateOfStart=" + dateOfStart +
      ", dateOfFinish=" + dateOfFinish +
      '}';
  }
}
